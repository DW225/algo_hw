#include <iostream>
#include <math.h>

using namespace std;
double rounding (double num, int index);

int main () {
  int runCount = 0;
  float studentCount = 0;
  float scores[1000] = {0};
  float score = 0;
  float sum = 0;
  float average = 0;
  int i = 0;
  int counter = 0;
  cin >> runCount;

  while (runCount != 0)
  {
    cin >> studentCount;
    for (i = 0; i < studentCount; i++){
      cin >> score;
      scores[i] = score;
      sum += score;
    }

    average = (sum / studentCount);
    for (i = 0; i < studentCount; i++){
      if(scores[i] > average){
        counter++;
      }
    }
    cout << rounding((counter/studentCount)*100, 3) << "%" << endl;
    runCount--;
  }
}

double rounding (double num, int index) {
  int multiplier;
  multiplier = pow(10, index);
  num = (int)(num * multiplier + 0.5) / (multiplier * 1.0);
  return num;
}