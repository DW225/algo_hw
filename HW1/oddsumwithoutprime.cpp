#include <iostream> 
using namespace std; 

// Find all prime numbers 
bool isPrime(int n) 
{
	bool isPrime = true;
	if (n == 0 || n == 1) {
        isPrime = false;
  }
  else {
    for (int i = 2; i <= n / 2; ++i) {
      if (n % i == 0) {
        isPrime = false;
        break;
      }
    }
  } 

	return isPrime; 
} 

// Function to find sum 
int find_Sum(int begin, int end) 
{ 
	// To store required answer 
	int sum = 0; 

	
	for (int i = begin; i <= end; i++) {
		if (i%2 == 1 && !isPrime(i)){
			sum += i;
		}
	}

	// Return the required answer 
	return sum; 
} 

// Driver code 
int main() 
{ 
	int a, b = 0; 
	int count = 0;
	cin >> count;
	while(count != 0){
		cin >> a >> b;
		cout << find_Sum(a, b) << endl; 
    count--;
	}
	

	return 0; 
} 
