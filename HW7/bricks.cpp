#include <iostream>

using namespace std;

int main()
{
    int N = 0; // number of test samples
    int n = 0; // number of stack of bricks
    int *brickStacks;
    int averageHigh = 0;
    int stepRequired = 0;
    cin >> N;
    while (N-- > 0)
    {
        cin >> n;
        brickStacks = new int[n];
        for (int i = 0; i < n; i++)
        {
            cin >> brickStacks[i];
        }

        for (int i = 0; i < n; i++)
        {
            averageHigh += brickStacks[i];
        }
        averageHigh = averageHigh/n;

        for (int i = 0; i < n; i++)
        {
            stepRequired += abs(averageHigh - brickStacks[i]);
        }
        stepRequired = stepRequired/2;

        cout << stepRequired << endl;
        stepRequired = 0;
        averageHigh = 0;
    }
}