#include <iostream> 
using namespace std;

// This function divides a by greatest divisible
//  power of b
int maxDivide(int a, int b)
{
    while (a % b == 0)
        a = a / b;
    return a;
}
 
// Function to check if a number is ugly or not
int isUgly(int no)
{
    no = maxDivide(no, 2);
    no = maxDivide(no, 3);
    no = maxDivide(no, 5);
 
    return (no == 1) ? 1 : 0;
}
 
// Function to get the nth ugly number
int getNthUglyNo(int n)
{
    
    int i = 1;
     
    // ugly number count
    int count = 1;
 
    // Check for all integers untill ugly count
    //  becomes n
    while (n > count) {
        i++;
        if (isUgly(i))
            count++;
    }
    return i;
}
 
// Driver Code
int main()
{
    int K;
    int N;
    // Function call
    cin >> K;
    while(K --> 0)
    {
        cin >> N;
        unsigned no = getNthUglyNo(N);
        cout << no << endl;
    }
}

