#include <iostream> 
using namespace std;

void pageCalculater(int);
int main() 
{
  int sets = 0;
  int user_count = 0; // user counted page total
  
  cin >> sets;
  while(sets --> 0)
  {
    cin >> user_count;
    pageCalculater(user_count);   
  }
}

void pageCalculater(int user_count)
{
  int page_sum = 0;
  int page_count = 0;
  int page_lost = 0;
  while(!(user_count < 0))
  {
    user_count -= page_count;
    if(!(user_count < 0))
    {
      page_count++;
    }
  }
  page_lost = abs(user_count);
  cout << page_count << " " << page_lost << endl;
}
