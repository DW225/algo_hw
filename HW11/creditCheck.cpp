#include<iostream>
#include<cstdio>
using namespace std;

int main(){
  int N;
  string s;
  int odd, even, temp;
  while( scanf( "%d", &N ) != EOF ){
    for( int i = 0 ; i < N ; i++ ){
      odd = 0;
      even = 0;

      for( int j = 0 ; j < 4 ; j++ ){
        cin >> s;
        odd += (s[1]-'0')+(s[3]-'0');
        even += ((s[0]-'0')*2)/10 + ((s[0]-'0')*2)%10 + ((s[2]-'0')*2)/10 + ((s[2]-'0')*2)%10;
      }

      if( (odd+even)%10 ) printf( "Invalid\n" );
      else printf( "Valid\n" );
    }
  }
  return 0;
} 