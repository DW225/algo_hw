#include <iostream>

using namespace std;

int main()
{
  int sets = 0;
  int a, b;
  int t = 0;
  a = 0;
  b = 0;

  cin >> sets;
  while (sets > 0)
  {
    cin >> a >> b;

    while (b != 0)
    {
      t = b;
      b = a % b;
      a = t;
    }
    cout << a << endl;

    sets--;
  }
}