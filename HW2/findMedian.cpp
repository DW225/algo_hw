#include<iostream>
#include<list>

using namespace std;

int main () {
  int sets = 0;
  int input = 0;
  list<int> nums;
  int median;

  cin >> sets;
  for (int i = 0; i < sets; i++) {
    cin >> input;
    nums.push_back(input);
    nums.sort();

    list<int>::iterator vi = nums.begin();
    if (nums.size()%2 == 0)
    {
      for (int i = 0; i < (nums.size()/2); i++)
      {
        vi++;
      }
      median = (*vi + *--vi)/2;
    }
    else
    {
      for (int i = 0; i < nums.size()/2; i++)
      {
        vi++;
      }
      median = *vi;
    }
    cout << median << endl;
  }
  
}