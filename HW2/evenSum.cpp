#include <iostream>

using namespace std;

int main() {
  int T = 0;
  int a, b;
  int sum = 0;
  a = 0;
  b = 0;

  cin >> T;
  while (T > 0) {
    cin >> a >> b;

    while(a <= b) {
      if(a%2 == 0)
        sum = sum + a;
      a++;
    }

    cout << sum << endl;
    sum = 0;
    T--;
  }
  
}