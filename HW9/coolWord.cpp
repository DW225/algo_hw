#include <iostream>
#include <algorithm>
#include <string>
#include <vector>

using namespace std;

int main()
{
    int n = 0; // test cases
    int m = 0; // number of words
    int coolWordsCount = 0;
    int i = 1;
    cin >> n;
    while (n-- > 0)
    {
        cin >> m;
        while (m-- > 0)
        {
            string word;
            int *counter = new int[26];
            vector<int> alphabetCount;
            cin >> word;
            for (auto ch : word)
            {
                counter[(int)ch - 97]++;
            }
            for (int j = 0; j < 26; j++)
            {
                if (counter[j] != 0)
                    alphabetCount.push_back(counter[j]);
            }
            sort(alphabetCount.begin(), alphabetCount.end());
            int size = alphabetCount.size();
            alphabetCount.erase(unique(alphabetCount.begin(), alphabetCount.end()), alphabetCount.end());
            if (alphabetCount.size() == size && size > 1)
                coolWordsCount++;
        }
        cout << "Case " << i << ": " << coolWordsCount << endl;
        coolWordsCount = 0;
        i++;
    }
}