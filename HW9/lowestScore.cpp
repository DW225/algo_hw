#include <iostream>
#include <algorithm>
#include <vector>
#include <string>

using namespace std;

int main()
{
    int N = 0;
    int studentCount = 0;
    int userInput = 0;
    int previousScore = 0;
    bool switchClass = false;
    vector<int> ClassAStudentsScores;
    vector<int> ClassBStudentsScores;
    string result;
    cin >> N;
    while (N-- > 0)
    {
        cin >> studentCount;
        for (int i = 0; i < studentCount; i++)
        {
            cin >> userInput;
            if (previousScore > userInput)
                switchClass = true;
            previousScore = userInput;
            if (!switchClass)
                ClassAStudentsScores.push_back(userInput);
            else
                ClassBStudentsScores.push_back(userInput);
        }

        int i = 0, j = 0;
        // cout << ClassAStudentsScores.size() << endl;
        // cout << ClassBStudentsScores.size() << endl;
        while (result.size() < 3)
        {
            if (ClassAStudentsScores[i] < ClassBStudentsScores[j])
            {
                result.append("A");
                ++i;
            }
            else if (ClassAStudentsScores[i] > ClassBStudentsScores[j])
            {
                result.append("B");
                ++j;
            }
        }
        cout << result << endl;
        ClassAStudentsScores.clear();
        ClassBStudentsScores.clear();
        switchClass = false;
        result.clear();
    }
}