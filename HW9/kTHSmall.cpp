#include <iostream>
#include <algorithm>

using namespace std;

int main()
{
    int N = 0;
    int k = 0;
    int scores[20] = {0};

    cin >> N;
    while (N-- > 0)
    {
        for (int i = 0; i < 20; i++)
        {
            cin >> scores[i];
        }
        sort(scores, scores+20);
        cin >> k;
        cout << scores[k-1] << endl;
    }
}