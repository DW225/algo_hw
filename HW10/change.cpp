#include <iostream>
#include <vector>

using namespace std;

class Changer
{
public:
    int coinChange(vector<int> &coins, int amount)
    {
        int Max = amount + 1;
        vector<int> dp(amount + 1, Max);
        dp[0] = 0;
        for (int i = 1; i <= amount; i++)
        {
            for (int j = 0; j < coins.size(); j++)
            {
                if (coins[j] <= i)
                {
                    dp[i] = min(dp[i], dp[i - coins[j]] + 1);
                }
            }
        }
        return dp[amount] > amount ? -1 : dp[amount];
    }
};

int main()
{
    Changer changer;
    int sets = 0;
    int N = 0;
    int S = 0;
    vector<int> bankNotes;
    int bankNote;
    cin >> sets;
    while (sets-- > 0)
    {
        cin >> N >> S;
        for (int i = 0; i < N; i++)
        {
            cin >> bankNote;
            bankNotes.push_back(bankNote);
        }
        cout << changer.coinChange(bankNotes, S) << endl;
        bankNotes.clear();
    }
}