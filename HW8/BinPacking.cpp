#include <iostream>
using namespace std;
// We have to return number of bins needed implementing next fit online algorithm
int nextFit(int weight1[], int m, int C)
{
    // Result (Count of bins) and remaining capacity in current bin are initialized.
    int res = 0, bin_rem = C;
    // We have to place elements one by one
    for (int i = 0; i < m; i++)
    {
        // If this element can't fit in current bin
        if (weight1[i] > bin_rem)
        {
            res++; // Use a new bin
            bin_rem = C - weight1[i];
        }
        else
            bin_rem -= weight1[i];
    }
    return res;
}
// Driver program
int main()
{
    int *weight1;
    int C = 1;
    int N = 0;
    cin >> N;
    weight1 = new int[N];
    cout << nextFit(weight1, N, C) << endl;
    return 0;
}