#include <iostream>
#include <string.h>
using namespace std;
int main(int argc, char *argv[])
{
    int N;
    string s, t;
    cin >> N;
    while (N-- > 0)
    {
        cin >> s >> t;
        int n = 0;
        for (int i = 0; i < t.length() && n < s.length(); i++)
            if (t[i] == s[n])
                n++;
        cout << ((n == s.length()) ? "Yes" : "No") << endl;
    }
    return 0;
}