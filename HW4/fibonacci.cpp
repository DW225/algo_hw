#include <iostream>

using namespace std;
int fib(int n);

int main()
{
    int k = 0;
    int n = 0;
    cin >> k;
    while (k --> 0)
    {
        cin >> n;
        cout << fib(n) << endl;
    }
}

int fib(int n)
{
    if (n <= 1)
        return n;
    return fib(n-1) + fib(n-2);
}