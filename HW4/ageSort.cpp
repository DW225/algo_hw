#include <iostream>
#include <list>

using namespace std;

int main()
{
    int n = 0;
    int userInput = 0;
    list<int> ageList;
    while (cin >> n)
    {
        if (n == 0) break;
        else
        {
            while (n --> 0)
            {
                cin >> userInput;
                ageList.push_back(userInput);
            }
            ageList.sort();

            list<int>::iterator vi;
            for (vi = ageList.begin(); vi != ageList.end(); vi++)
            {
                cout << *vi;
                if (vi != ageList.end())
                {
                    cout << " ";
                }
            }
            ageList.clear();
        }
        cout << endl;
    }
}