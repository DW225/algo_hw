#include <iostream>

using namespace std;

long long Calculater(int);

int main()
{
    int nSets = 0;
    int layerCount = 0;
    cin >> nSets;
    while (nSets --> 0)
    {
        int N = 0;
        cin >> N;
        layerCount = (1 + N)*((N + 1)/2)/2;
        cout << Calculater(layerCount) << endl;
    }
}

long long Calculater(int layers)
{
    long long lastNum = 0;
    lastNum = 2 * layers - 1;
    return lastNum * (lastNum - 2) * (lastNum -4);
}