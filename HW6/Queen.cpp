#include <iostream>
#include <cstdlib>
#include <cstdio>

using namespace std;

int main()
{
    int x1, y1, x2, y2;
    while (~scanf("%d%d%d%d", &x1, &y1, &x2, &y2) && x1)
    {
        if (x1 == x2 && y1 == y2)
            cout << "True" << endl;
        else if (x1 == x2 || y1 == y2 || abs(x1 - x2) == abs(y1 - y2))
            cout << "True" << endl;
        else
            cout << "False" << endl;
    }
    return 0;
}
