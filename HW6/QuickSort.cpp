#include <iostream>
#include <vector>
#include <array>
#include <algorithm>
#include <iterator>

using namespace std;

void quickSortStep(int[], int, int);
bool test(int[], int[], int);

int main()
{
    int n = 0;
    int input = 0;
    int pivot = 0;
    cin >> n;
    int *inputArray = new int[n];
    int *result = new int[n];
    bool finished = false;
    for (int i = 0; i < n; i++)
    {
        cin >> input;
        inputArray[i] = input;
        result[i] = input;
    }
    sort(result, result + n);

    for (int i = 0; i < n; i++)
    {
        cout << *(inputArray + i) << " ";
    }
    cout << endl;

    while (!finished)
    {
        quickSortStep(inputArray, n, pivot);
        for (int i = 0; i < n; i++)
        {
            cout << *(inputArray + i) << " ";
        }
        cout << endl;

        for (int i = 0; i < n - 1; i++)
        {
            if (inputArray[i] > inputArray[i + 1])
            {
                finished = false;
                break;
            }
        }

        int smaller = n - pivot - 1;
        for (int i = 1; i < n; i++)
        {
            if (inputArray[pivot] < inputArray[i])
            {
                smaller--;
            }
        }
        if (smaller == 0)
        {
            pivot++;
        }
        if (test(inputArray, result, n) || pivot == n - 1)
        {
            finished = true;
            break;
        }
    }

    return 0;
}

void quickSortStep(int arr[], int length, int pivot)
{
    vector<int> left, right;
    int pivotNumber = arr[pivot];
    for (int i = pivot + 1; i < length; i++)
    {
        if (arr[i] > arr[pivot])
        {
            left.push_back(arr[i]);
        }
        else
        {
            right.push_back(arr[i]);
        }
    }
    int i = pivot;
    for (const auto &it : right)
    {
        arr[i] = it;
        i++;
    }
    arr[i] = pivotNumber;
    i++;
    for (const auto &it : left)
    {
        arr[i] = it;
        i++;
    }
}

bool test(int arr[], int rsl[], int length)
{
    vector<int> temp1, temp2;
    for (int i = 0; i < length; i++)
    {
        temp1.push_back(arr[i]);
        temp2.push_back(rsl[i]);
    }
    if (temp1 == temp2)
        return true;
    else
        return false;
}