#include <iostream>

using namespace std;

bool tester(int, int);

int main ()
{
    int times = 0;
    int userInput = 0;
    int phi_n = 1;
    cin >> times;
    while (times --> 0)
    {
        cin >> userInput;
        for (int i = 2; i < userInput; i++)
        {
            if (!tester(userInput, i))
            {
                phi_n++;
            }
        }
        cout << phi_n << endl;
        phi_n = 1;
    }
}

bool tester(int a, int b)
{
    int temp = 0;
    while (b!=0)
    {
        temp = b;
        b = a%b;
        a = temp;
    }

    if (a != 1)
    {
        return true;
    }
    else
    {
        return false;
    }
}